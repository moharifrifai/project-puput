var oTable;
var lastID;
$(document).ready(function() {
    oTable = $('#dataTableA').DataTable({
        "ajax": {
            "type": "POST",
            "url": BASE_URL + controller + '/get',
            "data": function(d) {
                
            }
        },
        "columns": [
            { "data": "patient_cd" },
            { "data": "patient_rm" },
            { "data": "patient_name" },      
            { "data": "patient_age" },
            { "data": "patient_gender" },
            { "data": "patient_address" },
            { 
                "data": null,
                "render" : function(d){
                    return '<button type="button" class="btn btn-sm btn-warning" id="updateBtn" onclick="showEdit('+d.patient_id+')"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-sm btn-danger"  onclick="showDelete('+d.patient_id+')" id="delBtn"><i class="fas fa-trash"></i></button>';
                }
            }
        ],
        "responsive": true,
    });
    $("#addBtn").on("click", function(){
        setReadonly(false);
        reset_modal();
        set_id();
        $("#mode").val("create");
        $("#modalLabel").text("Add");
        $("#saveBtn").prop("hidden", false);
        $("#cancelBtn").prop("hidden", false);
        $("#dataModal").modal("show");
    });
    $("#saveBtn").on("click", function(){
        if(validate()){
            $("#saveBtn").text("Please Wait");
            $("#saveBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#changeBtn").on("click", function(){
        if(validate()){
            $("#changeBtn").text("Please Wait");
            $("#changeBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#deleteBtn").on("click", function(){
        if(validate()){
            $("#deleteBtn").text("Please Wait");
            $("#deleteBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#patient_cd").on("keypress", function(){
        $("#patient_cd").removeClass("is-invalid");
    });
    $("#patient_rm").on("keypress", function(){
        $("#patient_rm").removeClass("is-invalid");
    });
    $("#patient_name").on("keypress", function(){
        $("#patient_name").removeClass("is-invalid");
    });
    $("#patient_age").on("keypress", function(){
        $("#patient_age").removeClass("is-invalid");
    });
    $("#patient_address").on("keypress", function(){
        $("#patient_address").removeClass("is-invalid");
    });
    if($("#check_notif").val() == "create"){
        toast("bg-success", "Success", "Add data success!");
    }else if($("#check_notif").val() == "update"){
        toast("bg-success", "Success", "Update data success!");
    }else if($("#check_notif").val() == "delete"){
        toast("bg-success", "Success", "Delete data success!");
    }
});
function reset_modal(){
    $("#saveBtn").prop("hidden", true);
    $("#changeBtn").prop("hidden", true);
    $("#deleteBtn").prop("hidden", true);
    $("#cancelBtn").prop("hidden", true);
    $("#patient_cd").val("");
    $("#patient_rm").val("");
    $("#patient_name").val("");
    $("#patient_age").val("");
    $("#patient_address").val("");
    $("#patient_genderL").prop("checked", true);
}
function set_modal(patient_id){
    $.getJSON(BASE_URL + controller + '/get_by_id/'+patient_id, function (r) {
        $("#patient_id").val(r.data[0].patient_id);
        $("#patient_cd").val(r.data[0].patient_cd);
        $("#patient_rm").val(r.data[0].patient_rm);
        $("#patient_name").val(r.data[0].patient_name);
        $("#patient_age").val(r.data[0].patient_age);
        $("#patient_address").val(r.data[0].patient_address);        
        $("#patient_gender"+r.data[0].patient_gender).prop("checked", true);
    });
    $("#saveBtn").prop("hidden", true);
    $("#changeBtn").prop("hidden", true);
    $("#deleteBtn").prop("hidden", true);
    $("#cancelBtn").prop("hidden", true);
}
function validate(){
    a = false;
    if($("#patient_cd").val() != ""){
        if($("#patient_rm").val() != ""){
            if($("#patient_name").val() != ""){
                if($("#patient_age").val() != ""){
                    if($("#patient_address").val() != ""){
                        a = true;
                    }
                }
            }
        }
    }
    if($("#patient_cd").val() == ""){
        $("#patient_cd").addClass("is-invalid");
    }
    if($("#patient_rm").val() == ""){
        $("#patient_rm").addClass("is-invalid");
    }
    if($("#patient_name").val() == ""){
        $("#patient_name").addClass("is-invalid");
    }
    if($("#patient_age").val() == ""){
        $("#patient_age").addClass("is-invalid");
    }
    if($("#patient_address").val() == ""){
        $("#patient_address").addClass("is-invalid");
    }
    return a;
}
function set_id() {
    $.getJSON(BASE_URL + controller + '/get_last_id', function (data) {
        $("#patient_id").val(data.patient_id);
        $("#patient_cd").val(data.patient_cd);
    });
}
function numOnly(event) {
    var key = event.keyCode;
    return ((key > 47 && key < 58) || key == 8 || key == 9 || key == 46 );
};
function setReadonly(mode){
    $("#patient_rm").prop("readonly", mode);
    $("#patient_name").prop("readonly", mode);
    $("#patient_age").prop("readonly", mode);
    $("#patient_address").prop("readonly", mode);        
}
function showEdit(patient_id){
    setReadonly(false);
    set_modal(patient_id);
    $("#mode").val("update");
    $("#modalLabel").text("Update");
    $("#changeBtn").prop("hidden", false);
    $("#cancelBtn").prop("hidden", false);
    $("#dataModal").modal("show");
}
function showDelete(patient_id){
    setReadonly(true);
    set_modal(patient_id);
    $("#mode").val("delete");
    $("#modalLabel").text("Delete");
    $("#deleteBtn").prop("hidden", false);
    $("#cancelBtn").prop("hidden", false);
    $("#dataModal").modal("show");
}