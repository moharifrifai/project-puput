var oTable;
$(document).ready(function() {
    oTable = $('#dataTableA').DataTable({
        "ajax": {
            "type": "POST",
            "url": BASE_URL + controller + '/get',
            "data": function(d) {
            }
        },
        "columns": [
            { "data": "employe_cd" },
            { "data": "employe_name" },
            { "data": "employe_gender" },      
            { "data": "employe_age" },
            { 
                "data": null,
                "render" : function(d){
                    return '<button type="button" class="btn btn-sm btn-warning" onclick="showEdit('+d.employe_id+')" id="addBtn"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-sm btn-danger"  onclick="showDelete('+d.employe_id+')" id="addBtn"><i class="fas fa-trash"></i></button>';
                }
            }
        ],
        "responsive": true,
    });
    $("#saveBtn").on("click", function(){
        if(validate()){
            $("#saveBtn").text("Please Wait");
            $("#saveBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#addBtn").on("click", function(){
        setReadonly(false);
        reset_modal();
        set_id();
        $("#mode").val("create");
        $("#modalLabel").text("Add");
        $("#saveBtn").prop("hidden", false);
        $("#cancelBtn").prop("hidden", false);
        $("#dataModal").modal("show");
    });
    $("#changeBtn").on("click", function(){
        if(validate()){
            $("#changeBtn").text("Please Wait");
            $("#changeBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#deleteBtn").on("click", function(){
        if(validate()){
            $("#deleteBtn").text("Please Wait");
            $("#deleteBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#employe_name").on("keypress", function(){
        $("#employe_name").removeClass("is-invalid");
    });
    $("#employe_age").on("keypress", function(){
        $("#employe_age").removeClass("is-invalid");
    });
    if($("#check_notif").val() == "create"){
        toast("bg-success", "Success", "Add data success!");
    }else if($("#check_notif").val() == "update"){
        toast("bg-success", "Success", "Update data success!");
    }else if($("#check_notif").val() == "delete"){
        toast("bg-success", "Success", "Delete data success!");
    }
});
function validate(){
    a = false;
    if($("#employe_name").val() != ""){
        if($("#employe_age").val() != ""){
            a = true;                        
        }
    }    
    if($("#employe_name").val() == ""){
        $("#employe_name").addClass("is-invalid");
    }
    if($("#employe_age").val() == ""){
        $("#employe_age").addClass("is-invalid");
    }
    return a;
}
function set_id() {
    $.getJSON(BASE_URL + controller + '/get_last_id', function (data) {
        $("#employe_id").val(data.employe_id);
        $("#employe_cd").val(data.employe_cd);
    });
}
function setReadonly(mode){
    $("#employe_name").prop("readonly", mode);
    $("#employe_gender").prop("readonly", mode);
    $("#employe_age").prop("readonly", mode);     
}
function reset_modal(){
    $("#saveBtn").prop("hidden", true);
    $("#changeBtn").prop("hidden", true);
    $("#deleteBtn").prop("hidden", true);
    $("#cancelBtn").prop("hidden", true);
    $("#employe_cd").val("");
    $("#employe_name").val("");
    $("#employe_age").val("");
    $("#employe_genderL").prop("checked", true);
}
function set_modal(employe_id){
    $.getJSON(BASE_URL + controller + '/get_by_id/'+employe_id, function (r) {
        $("#employe_id").val(r.data[0].employe_id);
        $("#employe_cd").val(r.data[0].employe_cd);
        $("#employe_name").val(r.data[0].employe_name);
        $("#employe_age").val(r.data[0].employe_age);        
        $("#employe_gender"+r.data[0].employe_gender).prop("checked", true);
    });
    $("#saveBtn").prop("hidden", true);
    $("#changeBtn").prop("hidden", true);
    $("#deleteBtn").prop("hidden", true);
    $("#cancelBtn").prop("hidden", true);
}
function showEdit(employe_id){
    setReadonly(false);
    set_modal(employe_id);
    $("#mode").val("update");
    $("#modalLabel").text("Update");
    $("#changeBtn").prop("hidden", false);
    $("#cancelBtn").prop("hidden", false);
    $("#dataModal").modal("show");
}
function showDelete(employe_id){
    setReadonly(true);
    set_modal(employe_id);
    $("#mode").val("delete");
    $("#modalLabel").text("Delete");
    $("#deleteBtn").prop("hidden", false);
    $("#cancelBtn").prop("hidden", false);
    $("#dataModal").modal("show");
}