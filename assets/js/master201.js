var oTable;
$(document).ready(function() {
    oTable = $('#dataTableA').DataTable({
        "ajax": {
            "type": "POST",
            "url": BASE_URL + controller + '/get',
            "data": function(d) {
            }
        },
        "columns": [
            { "data": "doctor_cd" },
            { "data": "doctor_name" },
            { "data": "doctor_gender" },      
            { "data": "doctor_type" },
            { 
                "data": null,
                "render" : function(d){
                    return '<button type="button" class="btn btn-sm btn-warning"  onclick="showEdit('+d.doctor_id+')" id="addBtn"><i class="fas fa-edit"></i></button> <button type="button" class="btn btn-sm btn-danger" onclick="showDelete('+d.doctor_id+')" id="addBtn"><i class="fas fa-trash"></i></button>';
                }
            }
        ],
        "responsive": true,
    });
    $("#addBtn").on("click", function(){
        setReadonly(false);
        reset_modal();
        set_id();
        $("#mode").val("create");
        $("#modalLabel").text("Add");
        $("#saveBtn").prop("hidden", false);
        $("#cancelBtn").prop("hidden", false);
        $("#dataModal").modal("show");
    });
    $("#saveBtn").on("click", function(){
        if(validate()){
            $("#saveBtn").text("Please Wait");
            $("#saveBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#changeBtn").on("click", function(){
        if(validate()){
            $("#changeBtn").text("Please Wait");
            $("#changeBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#deleteBtn").on("click", function(){
        if(validate()){
            $("#deleteBtn").text("Please Wait");
            $("#deleteBtn").prop("disabled",true);
            $("#form").submit();
        }else{
            
        }
    });
    $("#doctor_name").on("keypress", function(){
        $("#doctor_name").removeClass("is-invalid");
    });
    $("#doctor_type").on("keypress", function(){
        $("#doctor_type").removeClass("is-invalid");
    });
    if($("#check_notif").val() == "create"){
        toast("bg-success", "Success", "Add data success!");
    }else if($("#check_notif").val() == "update"){
        toast("bg-success", "Success", "Update data success!");
    }else if($("#check_notif").val() == "delete"){
        toast("bg-success", "Success", "Delete data success!");
    }
});
function validate(){
    a = false;
    if($("#doctor_cd").val() != ""){
        if($("#doctor_name").val() != ""){
            if($("#doctor_type").val() != ""){
                a = true;
            }
        }
    }
    if($("#doctor_name").val() == ""){
        $("#doctor_name").addClass("is-invalid");
    }
    if($("#doctor_type").val() == ""){
        $("#doctor_type").addClass("is-invalid");
    } 
    return a;       
}
function reset_modal(){
    $("#saveBtn").prop("hidden", true);
    $("#changeBtn").prop("hidden", true);
    $("#deleteBtn").prop("hidden", true);
    $("#cancelBtn").prop("hidden", true);
    $("#doctor_name").val("");
    $("#doctor_genderL").prop("checked", true);
    $("#doctor_type").val("");
}
function set_modal(doctor_id){
    $.getJSON(BASE_URL + controller + '/get_by_id/'+doctor_id, function (r) {
        $("#doctor_id").val(r.data[0].doctor_id);
        $("#doctor_cd").val(r.data[0].doctor_cd);
        $("#doctor_name").val(r.data[0].doctor_name);
        $("#doctor_type").val(r.data[0].doctor_type); 
        $("#doctor_gender"+r.data[0].doctor_gender).prop("checked", true);
    });
    $("#saveBtn").prop("hidden", true);
    $("#changeBtn").prop("hidden", true);
    $("#deleteBtn").prop("hidden", true);
    $("#cancelBtn").prop("hidden", true);
}
function set_id() {
    $.getJSON(BASE_URL + controller + '/get_last_id', function (data) {
        $("#doctor_id").val(data.doctor_id);
        $("#doctor_cd").val(data.doctor_cd);
    });
}
function setReadonly(mode){
    $("#doctor_name").prop("readonly", mode);
    $("#doctor_gender").prop("readonly", mode);     
    $("#doctor_type").prop("readonly", mode);
}
function showEdit(doctor_id){
    setReadonly(false);
    set_modal(doctor_id);
    $("#mode").val("update");
    $("#modalLabel").text("Update");
    $("#changeBtn").prop("hidden", false);
    $("#cancelBtn").prop("hidden", false);
    $("#dataModal").modal("show");
}
function showDelete(doctor_id){
    setReadonly(true);
    set_modal(doctor_id);
    $("#mode").val("delete");
    $("#modalLabel").text("Delete");
    $("#deleteBtn").prop("hidden", false);
    $("#cancelBtn").prop("hidden", false);
    $("#dataModal").modal("show");
}