$(document).ready(function(){
    $("#user_name" ).keydown(function() {
        $("#user_name").removeClass("is-invalid");
    });
    $("#user_password" ).keydown(function() {
        $("#user_password").removeClass("is-invalid");
    });
    $('#btnLoginFb').click(function(){
    });

    $('#btnLogin').click(function(){
        if(validate()){
            $.ajax({
                url: BASE_URL + 'login',
                type: "POST",
                dataType: "JSON",
                async: false,
                data: {
                    user_name: $("#user_name").val()
                    , user_password: $("#user_password").val()
                    , remember: $("#remember").prop("checked")
                },
                success: function(r) {
                    if (r.status == "0") {
                        toast("bg-danger", "Failed", "Your username or email address is wrong!");
                    }else if(r.status == "1"){
                        session_register(r);                        
                    }
                }
            });
        }else{
            toast("bg-warning", "Warning", "Please fill mandatory fields!");
        }
    });

});
function validate(){
    a = false;
    if($("#user_name").val() != ""){
        if($("#user_password").val() != ""){
            a = true;
        }
    }
    if($("#user_name").val() == ""){
        $("#user_name").addClass("is-invalid");
    }
    if($("#user_password").val() == ""){
        $("#user_password").addClass("is-invalid");
    }
    return a;
}
function session_register(row){
    $.ajax({
        url: BASE_URL + 'session_register/json',
        type: "POST",
        dataType: "JSON",
        async: false,
        data: {
            row: row
        },
        success: function(r) {
            if (r == "0") {
                toast("bg-danger", "Failed", "Your username or email address is wrong!");
            }else if(r == "1"){
                window.location = BASE_URL + 'dashboard';
            }
        }
    });
}