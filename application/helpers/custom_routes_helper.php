<?php
    function auth($logged, $controller, $method, $isPermission){
        if (!$logged) {
            redirect('login');
        }else if(!$isPermission){
			redirect('permission_denied');
		}else{
			return true;
		}
    }