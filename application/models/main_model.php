<?php
class Main_model extends CI_Model
{
    function login($user_name, $user_password){
        $sql = "SELECT * FROM tb_user WHERE (user_name='". $user_name ."' OR user_email='".$user_name."') AND user_password='".$user_password."'";
        $data = $this->db->query($sql);
        return $data;
    }
    function getTitle($controller, $method){
        $sql = "SELECT title_id, title_name, controller, views FROM tb_title WHERE (controller='". $controller ."' AND method='".$method."') OR title_id='1'";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    function getByCookie($cookie)
    {
        $this->db->where('cookie', $cookie);
        return $this->db->get('tb_user');
    }
    function update_cookie($user_id, $arrData){
        $this->db->where("user_id", $user_id);
        $this->db->update("tb_user", $arrData);
    }
    function getNavbar($user_type_id){
        $sql = "SELECT A.title_type_id, A.title_type_name, B.title_id, B.title_name, B.controller, B.method, B.icon, B.seq  FROM tb_title_type A
        INNER JOIN tb_title B ON B.title_type_id = A.title_type_id
        INNER JOIN tb_user_permission C ON C.user_type_id = '".$user_type_id."'
        WHERE A.title_type_visible = 1 AND B.is_navbar = 1 AND C.title_id = B.title_id
        ORDER BY A.title_type_seq, B.seq";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    // function getController(){
    //     $sql = "SELECT controller FROM tb_title GROUP BY controller";
    //     $data = $this->db->query($sql)->result();
    //     return $data;
    // }
    function getPermission($user_type_id, $controller, $method){        
        $sql = "SELECT A.user_permission_id FROM tb_user_permission A 
        INNER JOIN tb_title B ON B.title_id = A.title_id
        WHERE A.user_type_id = '".$user_type_id."' AND B.controller='". $controller ."';
        ";
        $data = $this->db->query($sql)->result_array();
        if(isset($data)){
            if($data[0]['user_permission_id'] > 0){
                return true;
            }
        }else{
            return false;
        }
    }
}
