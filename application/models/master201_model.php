<?php
class Master201_model extends CI_Model
{
    public function get(){
        $sql = "SELECT * FROM tb_doctor";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    public function get_by_id($doctor_id){
        $sql = "SELECT * FROM tb_doctor WHERE doctor_id='".$doctor_id."'";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    public function mode(){
        if($_POST['mode']=="create"){
            $arrData = array(
                "doctor_id" => $_POST['doctor_id']
                ,"doctor_cd" => $_POST['doctor_cd']
                , "doctor_name" => $_POST['doctor_name']
                , "doctor_gender" => $_POST['doctor_gender']
                , "doctor_type" => $_POST['doctor_type']
            );
            $this->db->insert('tb_doctor', $arrData);
            if($this->db->affected_rows()){
                return $this->db->insert_id();
            }else{
                $msg['status'] = false;
                $msg['message'] = 'insert';
                return $msg;
            }
        }else if($_POST['mode']=="update"){
            $arrData = array(
                "doctor_cd" => $_POST['doctor_cd']
                , "doctor_name" => $_POST['doctor_name']
                , "doctor_gender" => $_POST['doctor_gender']
                , "doctor_type" => $_POST['doctor_type']
            );
            $this->db->where("doctor_id", $_POST['doctor_id']); 
            $this->db->update('tb_doctor', $arrData);
            if($this->db->affected_rows()){
                return $_POST['doctor_id'];
            }else{
                $msg['status'] = false;
                $msg['message'] = 'update';
                return $msg;
            }
        }else if($_POST['mode']=="delete"){
            $this->db->where("doctor_id", $_POST['doctor_id']); 
            $this->db->delete('tb_doctor');
            if($this->db->affected_rows()){
                return 0;
            }else{
                $msg['status'] = false;
                $msg['message'] = 'delete';
                return $msg;
            }
        }
    }
}
