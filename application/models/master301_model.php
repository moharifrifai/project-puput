<?php
class Master301_model extends CI_Model
{
    public function get(){
        $sql = "SELECT * FROM tb_employe";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    public function get_by_id($employe_id){
        $sql = "SELECT * FROM tb_employe WHERE employe_id='".$employe_id."'";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    public function mode(){
        if($_POST['mode']=="create"){
            $arrData = array(
                "employe_id" => $_POST['employe_id']
                ,"employe_cd" => $_POST['employe_cd']
                , "employe_name" => $_POST['employe_name']
                , "employe_gender" => $_POST['employe_gender']
                , "employe_age" => $_POST['employe_age']
            );
            $this->db->insert('tb_employe', $arrData);
            if($this->db->affected_rows()){
                return $this->db->insert_id();
            }else{
                $msg['status'] = false;
                $msg['message'] = 'insert';
                return $msg;
            }
        }else if($_POST['mode']=="update"){
            $arrData = array(
                "employe_cd" => $_POST['employe_cd']
                , "employe_name" => $_POST['employe_name']
                , "employe_gender" => $_POST['employe_gender']
                , "employe_age" => $_POST['employe_age']
            );
            $this->db->where("employe_id", $_POST['employe_id']); 
            $this->db->update('tb_employe', $arrData);
            if($this->db->affected_rows()){
                return $_POST['employe_id'];
            }else{
                $msg['status'] = false;
                $msg['message'] = 'update';
                return $msg;
            }
        }else if($_POST['mode']=="delete"){
            $this->db->where("employe_id", $_POST['employe_id']); 
            $this->db->delete('tb_employe');
            if($this->db->affected_rows()){
                return 0;
            }else{
                $msg['status'] = false;
                $msg['message'] = 'delete';
                return $msg;
            }
        }
    }
}
