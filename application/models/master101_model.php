<?php
class Master101_model extends CI_Model
{
    public function get(){
        $sql = "SELECT * FROM tb_patient";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    public function get_by_id($patient_id){
        $sql = "SELECT * FROM tb_patient WHERE patient_id='".$patient_id."'";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    public function mode(){
        if($_POST['mode']=="create"){
            $arrData = array(
                "patient_id" => $_POST['patient_id']
                ,"patient_cd" => $_POST['patient_cd']
                , "patient_rm" => $_POST['patient_rm']
                , "patient_name" => $_POST['patient_name']
                , "patient_age" => $_POST['patient_age']
                , "patient_gender" => $_POST['patient_gender']
                , "patient_address" => $_POST['patient_address']
            );
            $this->db->insert('tb_patient', $arrData);
            if($this->db->affected_rows()){
                return $this->db->insert_id();
            }else{
                $msg['status'] = false;
                $msg['message'] = 'insert';
                return $msg;
            }
        }else if($_POST['mode']=="update"){
            $arrData = array(
                "patient_cd" => $_POST['patient_cd']
                , "patient_rm" => $_POST['patient_rm']
                , "patient_name" => $_POST['patient_name']
                , "patient_age" => $_POST['patient_age']
                , "patient_gender" => $_POST['patient_gender']
                , "patient_address" => $_POST['patient_address']
            );
            $this->db->where("patient_id", $_POST['patient_id']); 
            $this->db->update('tb_patient', $arrData);
            if($this->db->affected_rows()){
                return $_POST['patient_id'];
            }else{
                $msg['status'] = false;
                $msg['message'] = 'update';
                return $msg;
            }
        }else if($_POST['mode']=="delete"){
            $this->db->where("patient_id", $_POST['patient_id']); 
            $this->db->delete('tb_patient');
            if($this->db->affected_rows()){
                return 0;
            }else{
                $msg['status'] = false;
                $msg['message'] = 'delete';
                return $msg;
            }
        }
    }
}
