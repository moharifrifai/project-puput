  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?=base_url()?>logout">Logout</a>
        </div>
      </div>
    </div>
  </div>

    <div id="polite" aria-live="polite" tabindex="-1" aria-atomic="true" style="position: absolute; top: 50px; right: 50px;">
        <div id="toast1" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-animation="true" data-delay="5000" data-autohide="true">
            <div class="toast-header">
                <span  class="rounded mr-2 toast-color" style="width: 15px;height: 15px"></span>
                <strong class="mr-auto toast-title">Notifikasi</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
              a
            </div>
        </div>
    </div>

  <!-- Bootstrap core JavaScript-->
  
  <script src="<?php echo base_url();?>assets/js/jquery.js"></script> 
	<script src="<?php echo base_url();?>assets/js/popper.js"></script> 
  <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?php echo base_url();?>assets/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>assets/js/sb-admin-2.min.js"></script>
  <script>
     function toast(toastColor, toastTitle, toastBody){
        var $div = $('div[id^="toast"]:last');
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 );
        var Nextnum = num+1;
        $("#toast"+num).clone().prop('id', 'toast'+Nextnum ).appendTo("#polite");
        $("#toast"+num+" .toast-header .toast-color").addClass(toastColor)
        $("#toast"+num+" .toast-header .toast-title").text(toastTitle)
        $("#toast"+num+" .toast-body").text(toastBody)
        $('#toast'+num).toast('show');
    }
  </script>
  <script>
      var BASE_URL = "<?php echo base_url();?>";
      var controller = "<?php echo $controller;?>";
  </script>
  <?php
    if(isset($arrJs)){
      foreach($arrJs as $js){
        ?>
          <script src="<?php echo base_url();?>assets/js/<?= $js;?>.js"></script>
        <?php
      }
  
    }
  ?>
  <script>
	</script>
</body>

</html>