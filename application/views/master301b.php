
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?=$title_name?></h1>
        <button type="button" class="btn btn-success" id="addBtn"><i class="fas fa-plus"></i></button>
    </div>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="dataTableA" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Kode</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Spesialis</th>
            <th width="55px"></th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->
<!-- Logout Modal-->
<div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalLabel"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
        <form>
            <div class="form-group row">
                <label for="patient_cd" class="col-sm-4 col-form-label col-form-label">Kode Pasien</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="patient_cd" name="patient_cd">
                </div>
            </div>
            <div class="form-group row">
                <label for="patient_rm" class="col-sm-4 col-form-label col-form-label">No. RM</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="patient_rm">
                </div>
            </div>
            <div class="form-group row">
                <label for="patient_name" class="col-sm-4 col-form-label col-form-label">Nama</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="patient_name">
                </div>
            </div>
            <div class="form-group row">
                <label for="patient_age" class="col-sm-4 col-form-label col-form-label">Umur</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="patient_age">
                </div>
            </div>
            <div class="form-group row">
                <label for="patient_gender" class="col-sm-4 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-8">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="patient_gender" id="patient_genderL" value="L" checked>
                        <label class="col-form-label" for="patient_genderL"><?=gender("L")?></label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="patient_gender" id="patient_genderP" value="P">
                        <label class="col-form-label" for="patient_genderP"><?=gender("P")?></label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="patient_address" class="col-sm-4 col-form-label col-form-label">Alamat</label>
                <div class="col-sm-8">
                  <textarea class="form-control form-control" name="patient_address" id="patient_address"></textarea>
                </div>
            </div>
        </form>  

        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="button" id="saveBtn">Save</button>
          <button class="btn btn-warning" type="button" id="changeBtn">Change</button>
          <button class="btn btn-danger" type="button" id="deleteBtn">Delete</button>
          <button class="btn btn-secondary" type="button" data-dismiss="modal" id="cancelBtn">Cancel</button>
        </div>
      </div>
    </div>
  </div>