<?php include_once "header.php";?>
<!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include_once "sidebar.php";?>
    

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
        
        <!-- Main Content -->
        <div id="content">
            
            <!-- Sidebar -->
            <?php include_once "toolbar.php";?>

            <?php 
                if(isset($views)){
                    include_once $views.".php";
                }
            ?>
     
        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2019</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

  


<!-- Page level plugins -->
<script src="<?=base_url()?>assets/vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="<?=base_url()?>assets/js/demo/chart-area-demo.js"></script>
<script src="<?=base_url()?>assets/js/demo/chart-pie-demo.js"></script>
<?php include_once "footer.php";?>