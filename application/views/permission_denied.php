<?php include_once "header.php";?>
<style>
    .bg-password-image{
        background: url("<?=base_url();?>assets/img/bg/password.jpg");
        background-position: center;
        background-size: cover;
    }
</style>
    <div class="container">
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="container-fluid">

                                        <!-- 404 Error Text -->
                                        <div class="text-center">
                                            <div class="error mx-auto" data-text="007" style="font-size:6.9rem;">007</div>
                                            <p class="lead text-gray-800 mb-5">Permission Denied </p>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="<?=base_url()?>register">Create an Account!</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="<?=base_url()?>login">Already have an account? Login!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

<?php include_once "footer.php";?>