<input type="text" id="check_notif" value="<?=$mode?>" hidden>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?=$title_name?></h1>
        <button type="button" class="btn btn-success" id="addBtn"><i class="fas fa-plus"></i></button>
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTableA" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Spesialis</th>
                            <th width="55px"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
<!-- Logout Modal-->
<div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" id="form" autocomplete="off">
                    <input type="text" class="form-control" id="mode" name="mode" hidden>
                    <input type="text" class="form-control" id="doctor_id" name="doctor_id" hidden>

                    <div class="form-group row">
                        <label for="doctor_cd" class="col-sm-4 col-form-label col-form-label">Kode Dokter</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="doctor_cd" name="doctor_cd" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="doctor_name" class="col-sm-4 col-form-label col-form-label">Nama</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="doctor_name" name="doctor_name" style="text-transform:capitalize;">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="doctor_gender" class="col-sm-4 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-8">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="doctor_gender" id="doctor_genderL" value="L" checked>
                                <label class="col-form-label" for="doctor_genderL"><?=gender("L")?></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="doctor_gender" id="doctor_genderP" value="P">
                                <label class="col-form-label" for="doctor_genderP"><?=gender("P")?></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="doctor_type" class="col-sm-4 col-form-label col-form-label">Spesialis</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="doctor_type" name="doctor_type" style="text-transform:capitalize;">
                        </div>
                    </div>
                </form>  
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="saveBtn">Save</button>
                <button class="btn btn-warning" type="button" id="changeBtn">Change</button>
                <button class="btn btn-danger" type="button" id="deleteBtn">Delete</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal" id="cancelBtn">Cancel</button>
            </div>
        </div>
    </div>
</div>