<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=base_url()?>">
        <div class="sidebar-brand-text mx-3"><?=$brand?></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <?php
        $navbar_type = "None";
        foreach($navbar as $nb){
            if($nb['controller'] == $controller){
                $title_name = $nb['title_name'];
            }
            if($navbar_type != $nb['title_type_name']){
                ?>
                    <!-- Divider -->
                    <hr class="sidebar-divider">

                    <!-- Heading -->
                    <div class="sidebar-heading">
                        <?=$nb['title_type_name']?>
                    </div>
                <?php
                $navbar_type = $nb['title_type_name'];
            }
            ?>
                <!-- Nav Item - Dashboard -->
                <li class="nav-item <?php echo ($nb['controller'] == $controller ? "active" : "");?>">
                    <a class="nav-link" href="<?=base_url()?><?=$nb['controller']?>">
                         <?=$nb['icon']?>
                        <span><?=$nb['title_name']?></span>
                    </a>
                </li>
            <?php
        }
    ?>
    
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
