<?php include_once "header.php";?>
<style>
    .bg-login-image{
        background: url("<?=base_url();?>assets/img/bg/login.jpg");
        background-position: center;
    background-size: cover;
    }
</style>
<div class="container">
    
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                </div>
                                <form class="user">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user" name="user_name" id="user_name" aria-describedby="emailHelp" placeholder="Username or Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user" name="user_password" id="user_password" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input type="checkbox" class="custom-control-input" name="remember" id="remember">
                                            <label class="custom-control-label" for="remember">Remember Me</label>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary btn-user btn-block" id="btnLogin">Login</button>
                                    <hr>
                                    <button type="button" class="btn btn-google btn-user btn-block" id="btnLoginFb" disabled><i class="fab fa-google fa-fw"></i> Login with Google</button>
                                    <button type="button" class="btn btn-facebook btn-user btn-block" disabled><i class="fab fa-facebook-f fa-fw"></i> Login with Facebook</button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="<?= base_url()?>forgotPassword">Forgot Password?</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="<?= base_url()?>register">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
		

<?php include_once "footer.php";?>