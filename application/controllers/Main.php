<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct($user_id = FALSE)
	{		
		parent::__construct();
		$this->load->model('main_model', 'mdl');
		global $data;
		$method = $this->uri->segment(1);
		if($method =="m"){
			echo "A";
		}
		$title = $this->mdl->getTitle("main", $method);
		$this->data['controller'] = "main";
		$this->data['title'] = (count($title) == 1 ? $title[0]['title_name']:$title[0]['title_name']." - ".$title[1]['title_name']);
		// $routes = array_reverse($this->router->routes); 
		// $r = array_keys($routes, "main/$0");
		// $sbstr1 = substr($r[0], 4);
		// $sbstr2 = substr($sbstr1, 0, -3);
		// print_r(explode("|", $sbstr2));
	}
	public function master401()
	{
		echo "a";
	}
	public function check_login()
	{
		$cookie = get_cookie('userCookie');
		if ($this->session->userdata('logged')) {
            redirect(base_url() . 'dashboard');
        } else if($cookie <> '') {
            $row = $this->mdl->getByCookie($cookie)->row();
            if ($row) {
                $this->session_register($row);
            }else{
                echo "else";
            }
        } else {
            redirect(base_url() . "login");
        }
	}
	public function index()
	{
		$this->check_login();
	}
	public function login()
	{
		delete_cookie('userCookie');
        $this->session->sess_destroy();
		$this->data['arrJs'] = array("main_login");
		$user_name = ($this->input->post('user_name') ? $this->input->post('user_name') : "");
        $user_password = ($this->input->post('user_password') ? $this->input->post('user_password') : "");
		$remember = ($this->input->post('remember') ? $this->input->post('remember') : "");
		if($user_name != "" && $user_password != ""){
			$row = $this->mdl->login($user_name, $user_password)->row();
			if ($row) {
				if ($remember == "true") {
					$cookie = random_string('alnum', 64);
					set_cookie('userCookie', $cookie, 3600*24*30); // set expired 30 hari kedepan
					
					$arrData = array(
						'cookie' => $cookie
					);
					$this->mdl->update_cookie($row->user_id, $arrData);
				}
				$result['row'] = $row;
				$result['status'] = true;
			} else {
				$result['status'] = false;
			}
			echo json_encode($result);
		}else{
			$this->load->view('main_login', $this->data);
		}
	}
	public function register()
	{
		delete_cookie('userCookie');
        $this->session->sess_destroy();
		$this->load->view('main_register', $this->data);
	}
	public function forgotPassword()
	{
		delete_cookie('userCookie');
        $this->session->sess_destroy();
		$this->load->view('main_forgotPassword', $this->data);
	}
	public function session_register($rowGet) {
		if($rowGet=="json"){
			$row = ($this->input->post('row') ? $this->input->post('row') : "");
			if($row != ""){
				if($row['status']==true){
					$row = $row['row'];
				}
			}
		}else{
			$row = $rowGet;
		}
		if(isset($row)){
			$sess = array(
				'logged' => TRUE
				,'user_id' => $row['user_id']
				,'user_name' => $row['user_name']
				,'user_full_name' => $row['user_full_name']
				,'user_type_id' => $row['user_type_id']
			);
			$this->session->set_userdata($sess);
			$result = true;
		}else{
			$result = false;
		}
		echo json_encode($result);
	}
	public function logout()
    {
        delete_cookie('userCookie');
        $this->session->sess_destroy();
        redirect(base_url() . 'login');
	}
	public function error()
    {
        $this->load->view("404", $this->data);
	}
	public function permission_denied()
    {
        $this->load->view("permission_denied", $this->data);
	}
}
