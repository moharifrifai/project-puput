<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master101 extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		$isAjaxRequest = false;
		$controller = ($this->uri->segment(1) != "" ? $this->uri->segment(1) : null);
		$method = ($this->uri->segment(2) != "" ? $this->uri->segment(2) : null);

		$this->load->model($controller."_model", 'mdl');
		if(
			isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') == 0
		){
			//Set our $isAjaxRequest to true.
			$isAjaxRequest = true;
		}
		if(!$isAjaxRequest){
			global $data;
			$user_type_id = $this->session->userdata("user_type_id");
			$this->load->model("main_model", 'main');
			$isPermission = $this->main->getPermission($user_type_id, $controller, $method);
			$auth = auth($this->session->userdata('logged'), $controller, $method, $isPermission);
			if($auth){
				$title = $this->main->getTitle($controller, $method);
				$this->data['brand'] = $title[0]['title_name'];
				$this->data['title'] = (count($title) == 1 ? $title[0]['title_name']:$title[0]['title_name']." - ".$title[1]['title_name']);
				$this->data['navbar'] = $this->main->getNavbar($user_type_id);
				$this->data['controller'] = $controller;
				$this->data['views'] = $title[1]['views'];
			}
		}
		
		
    }
	public function index()
	{
		$this->data['mode']  = '';	
		if(isset($_POST['mode'])){
			$id = $this->mdl->mode();
			$this->data['mode'] = $_POST['mode'];
		}
		$this->data['arrJs'] = array($this->data['controller']);
		$this->load->view("template", $this->data);	
	}
	public function get()
	{
		$data = $this->mdl->get();
        
        $result['total'] = count($data);
        
        $rows = array();
        
        if(count($data) > 0){
            foreach($data as $dt){
                $r = array(
                    'patient_id'    => $dt['patient_id'],
                    'patient_cd'    => $dt['patient_cd'],
                    'patient_rm'   => $dt['patient_rm'],
                    'patient_name'   => $dt['patient_name'],
                    'patient_age'         => $dt['patient_age'],
                    'patient_gender'         => gender($dt['patient_gender']),
                    'patient_address'         => $dt['patient_address'],
                );
                $rows[] = $r;
            }
        }

        $result['data'] = $rows;

		echo json_encode($result);
	}
	public function get_last_id()
	{
		$this->load->database();
		$this->db->select('patient_id');
		$last_row = $this->db->order_by('patient_id',"desc")
					->limit(1)
					->get('tb_patient')
					->row();
		$patient_id = $last_row->patient_id + 1;
		$patient_cd = "P000";
		if(strlen($patient_id)>3){
			$data['patient_cd'] = "P".$patient_id;
		}else{
			$data['patient_cd'] = substr($patient_cd, 0, -strlen($patient_id)).$patient_id;
		}
		$data['patient_id'] = $patient_id;
		echo json_encode($data);
	}
	public function get_by_id($patient_id)
	{
		$data = $this->mdl->get_by_id($patient_id);
        
        $result['total'] = count($data);
        
        $rows = array();
        
        if(count($data) > 0){
            foreach($data as $dt){
                $r = array(
                    'patient_id'    => $dt['patient_id'],
                    'patient_cd'    => $dt['patient_cd'],
                    'patient_rm'   => $dt['patient_rm'],
                    'patient_name'   => $dt['patient_name'],
                    'patient_age'         => $dt['patient_age'],
                    'patient_gender'         => $dt['patient_gender'],
                    'patient_address'         => $dt['patient_address'],
                );
                $rows[] = $r;
            }
        }
        $result['data'] = $rows;

		echo json_encode($result);
	}
}
