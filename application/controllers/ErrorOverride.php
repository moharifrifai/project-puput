<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ErrorOverride extends CI_Controller {
    public function __construct() {
		parent::__construct();
    }
	public function index() {
        echo 
        $this->load->view("404");
    }
	public function error()
    {
        $this->load->view("404", $this->data);
	}
	public function permission_denied()
    {
        $this->load->view("permission_denied", $this->data);
	}
}