<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct() {
		parent::__construct();
		global $data;
		$user_type_id = $this->session->userdata("user_type_id");
		$controller = ($this->uri->segment(1) != "" ? $this->uri->segment(1) : null);
		$method = ($this->uri->segment(2) != "" ? $this->uri->segment(2) : null);
		$this->load->model("main_model", 'main');
		$isPermission = $this->main->getPermission($user_type_id, $controller, $method);
		$auth = auth($this->session->userdata('logged'), $controller, $method, $isPermission);
		if($auth){
			$title = $this->main->getTitle($controller, $method);
			$this->data['brand'] = $title[0]['title_name'];
			$this->data['title'] = (count($title) == 1 ? $title[0]['title_name']:$title[0]['title_name']." - ".$title[1]['title_name']);
			$this->data['navbar'] = $this->main->getNavbar($user_type_id);
			$this->data['controller'] = $controller;
			$this->data['views'] = $title[1]['views'];
			$this->load->model($controller."_model", 'mdl');
		}
		
		
    }
	public function index()
	{
		$this->load->view("template", $this->data);
	}
}
