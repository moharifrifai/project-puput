<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master201 extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		$isAjaxRequest = false;
		$controller = ($this->uri->segment(1) != "" ? $this->uri->segment(1) : null);
		$method = ($this->uri->segment(2) != "" ? $this->uri->segment(2) : null);

		$this->load->model($controller."_model", 'mdl');
		if(
			isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') == 0
		){
			//Set our $isAjaxRequest to true.
			$isAjaxRequest = true;
		}
		if(!$isAjaxRequest){
			global $data;
			$user_type_id = $this->session->userdata("user_type_id");
			$this->load->model("main_model", 'main');
			$isPermission = $this->main->getPermission($user_type_id, $controller, $method);
			$auth = auth($this->session->userdata('logged'), $controller, $method, $isPermission);
			if($auth){
				$title = $this->main->getTitle($controller, $method);
				$this->data['brand'] = $title[0]['title_name'];
				$this->data['title'] = (count($title) == 1 ? $title[0]['title_name']:$title[0]['title_name']." - ".$title[1]['title_name']);
				$this->data['navbar'] = $this->main->getNavbar($user_type_id);
				$this->data['controller'] = $controller;
				$this->data['views'] = $title[1]['views'];
			}
		}
		
		
    }
	public function index()
	{
		$this->data['mode']  = '';	
		if(isset($_POST['mode'])){
			$id = $this->mdl->mode();
			$this->data['mode'] = $_POST['mode'];
		}
		$this->data['arrJs'] = array($this->data['controller']);
		$this->load->view("template", $this->data);
	}
	public function get()
	{
		$data = $this->mdl->get();
        
        $result['total'] = count($data);
        
        $rows = array();
        
        if(count($data) > 0){
            foreach($data as $dt){
                $r = array(
                    'doctor_id'    => $dt['doctor_id'],
                    'doctor_cd'    => $dt['doctor_cd'],
                    'doctor_name'   => $dt['doctor_name'],
                    'doctor_gender'         => gender($dt['doctor_gender']),
                    'doctor_type'         => $dt['doctor_type'],
                );
                $rows[] = $r;
            }
        }

        $result['data'] = $rows;

		echo json_encode($result);
	}
	public function get_last_id()
	{
		$this->load->database();
		$this->db->select('doctor_id');
		$last_row = $this->db->order_by('doctor_id',"desc")
					->limit(1)
					->get('tb_doctor')
					->row();
		$doctor_id = $last_row->doctor_id + 1;
		$doctor_cd = "D000";
		if(strlen($doctor_id)>3){
			$data['doctor_cd'] = "P".$doctor_id;
		}else{
			$data['doctor_cd'] = substr($doctor_cd, 0, -strlen($doctor_id)).$doctor_id;
		}
		$data['doctor_id'] = $doctor_id;
		echo json_encode($data);
	}
	public function get_by_id($doctor_id)
	{
		$data = $this->mdl->get_by_id($doctor_id);
        
        $result['total'] = count($data);
        
        $rows = array();
        
        if(count($data) > 0){
            foreach($data as $dt){
                $r = array(
                    'doctor_id'    => $dt['doctor_id'],
                    'doctor_cd'    => $dt['doctor_cd'],
                    'doctor_name'   => $dt['doctor_name'],
                    'doctor_gender'         => $dt['doctor_gender'],
                    'doctor_type'         => $dt['doctor_type'],
                );
                $rows[] = $r;
            }
        }
        $result['data'] = $rows;

		echo json_encode($result);
	}

}
