/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : project-puput

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2019-11-12 17:00:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tb_doctor`
-- ----------------------------
DROP TABLE IF EXISTS `tb_doctor`;
CREATE TABLE `tb_doctor` (
  `doctor_id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_cd` varchar(255) DEFAULT NULL,
  `doctor_name` varchar(255) DEFAULT NULL,
  `doctor_gender` varchar(255) DEFAULT NULL,
  `doctor_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`doctor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_doctor
-- ----------------------------
INSERT INTO `tb_doctor` VALUES ('1', 'D001', 'dr. Cessy W.P., Sp.M', 'P', 'Mata');
INSERT INTO `tb_doctor` VALUES ('2', 'D002', 'dr. Verna, Sp.M', 'P', 'Mata');
INSERT INTO `tb_doctor` VALUES ('3', 'D003', 'dr. A. Choliq, Sp.M', 'L', 'Mata');

-- ----------------------------
-- Table structure for `tb_employe`
-- ----------------------------
DROP TABLE IF EXISTS `tb_employe`;
CREATE TABLE `tb_employe` (
  `employe_id` int(11) NOT NULL AUTO_INCREMENT,
  `employe_cd` varchar(255) DEFAULT NULL,
  `employe_name` varchar(255) DEFAULT NULL,
  `employe_gender` varchar(255) DEFAULT NULL,
  `employe_age` int(11) DEFAULT NULL,
  PRIMARY KEY (`employe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_employe
-- ----------------------------
INSERT INTO `tb_employe` VALUES ('1', 'E001', 'Fuji S.', 'P', '25');
INSERT INTO `tb_employe` VALUES ('2', 'E002', 'Putri Intan', 'P', '24');
INSERT INTO `tb_employe` VALUES ('3', 'E003', 'Abdulaziz', 'L', '30');
INSERT INTO `tb_employe` VALUES ('4', 'E004', 'Elvandari', 'P', '28');
INSERT INTO `tb_employe` VALUES ('5', 'E005', 'Tritini', 'P', '33');

-- ----------------------------
-- Table structure for `tb_patient`
-- ----------------------------
DROP TABLE IF EXISTS `tb_patient`;
CREATE TABLE `tb_patient` (
  `patient_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_cd` varchar(255) DEFAULT NULL,
  `patient_rm` varchar(255) DEFAULT NULL,
  `patient_name` varchar(255) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `patient_gender` varchar(255) DEFAULT NULL,
  `patient_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_patient
-- ----------------------------
INSERT INTO `tb_patient` VALUES ('121', 'P001', '3628', 'Karni', '52', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('122', 'P002', '1749', 'Supian', '58', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('123', 'P003', '2156', 'Tasima', '70', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('124', 'P004', '2099', 'Muzaryanah', '64', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('125', 'P005', '1496', 'Taslim', '62', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('126', 'P006', '3522', 'Uniya', '62', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('127', 'P007', '3215', 'Rueni', '57', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('128', 'P008', '1348', 'Alimin', '58', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('129', 'P009', '3402', 'Salima', '72', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('130', 'P010', '3450', 'Wagiman', '62', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('131', 'P011', '2792', 'Satya', '68', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('132', 'P012', '2939', 'Saniti', '62', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('133', 'P013', '2015', 'Uman', '64', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('134', 'P014', '1658', 'Misna', '59', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('135', 'P015', '3218', 'Iman', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('136', 'P016', '3423', 'Sami', '69', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('137', 'P017', '3876', 'El Yanto', '52', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('138', 'P018', '3319', 'Endang', '29', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('139', 'P019', '3578', 'Sunoyo', '25', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('140', 'P020', '2663', 'Adnan Maulana', '6', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('141', 'P021', '3758', 'Renin', '53', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('142', 'P022', '1504', 'Machrus', '52', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('143', 'P023', '1772', 'Sutina', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('144', 'P024', '2571', 'Nur Hanisa', '19', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('145', 'P025', '3581', 'Samroh', '52', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('146', 'P026', '3194', 'Rapiah', '69', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('147', 'P027', '2496', 'Karyo', '65', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('148', 'P028', '2017', 'Jaja K', '45', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('149', 'P029', '1913', 'Sojan', '63', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('150', 'P030', '3967', 'Bagiah', '73', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('151', 'P031', '3087', 'Catur Suganda', '60', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('152', 'P032', '3401', 'Astari', '68', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('153', 'P033', '2444', 'Nurrinta', '55', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('154', 'P034', '2325', 'Euis', '63', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('155', 'P035', '3213', 'Suganda', '62', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('156', 'P036', '1146', 'Sapiah', '59', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('157', 'P037', '2781', 'Juniah', '64', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('158', 'P038', '1851', 'Sariah', '52', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('159', 'P039', '2697', 'Damin', '56', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('160', 'P040', '2763', 'Maemunah', '72', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('161', 'P041', '3830', 'Sumarsono', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('162', 'P042', '2924', 'Nenci', '55', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('163', 'P043', '3452', 'Sonawo', '66', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('164', 'P044', '1710', 'Darsani', '67', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('165', 'P045', '1415', 'Cucu', '73', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('166', 'P046', '2714', 'Hasanah', '49', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('167', 'P047', '3625', 'Slamet', '61', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('168', 'P048', '1200', 'Sujita', '58', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('169', 'P049', '1489', 'Sugiyarto', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('170', 'P050', '3759', 'Edi M.', '71', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('171', 'P051', '2418', 'Miftahudin', '60', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('172', 'P052', '2144', 'Asmiri', '68', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('173', 'P053', '3601', 'Dendi', '64', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('174', 'P054', '1196', 'Yayah A.', '67', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('175', 'P055', '1213', 'Supandi', '33', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('176', 'P056', '1429', 'Mustinah', '52', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('177', 'P057', '3333', 'Juriah', '26', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('178', 'P058', '3297', 'Sarinah', '65', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('179', 'P059', '3006', 'Sanawi', '59', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('180', 'P060', '1860', 'Bahrudin', '57', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('181', 'P061', '1614', 'Amah Salamah', '59', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('182', 'P062', '2123', 'Turiah', '59', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('183', 'P063', '1438', 'Rama', '53', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('184', 'P064', '1435', 'Gunawan', '73', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('185', 'P065', '3696', 'Rastini', '46', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('186', 'P066', '1963', 'Layinah', '63', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('187', 'P067', '3466', 'Runiah', '68', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('188', 'P068', '2937', 'Sukarel', '38', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('189', 'P069', '1050', 'Sugiyati', '62', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('190', 'P070', '1905', 'Achmad', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('191', 'P071', '1714', 'Sugiyati', '73', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('192', 'P072', '1069', 'Dading', '61', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('193', 'P073', '2325', 'Sapin', '57', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('194', 'P074', '3790', 'Arjo', '53', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('195', 'P075', '2870', 'Madira', '48', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('196', 'P076', '2146', 'Toin', '43', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('197', 'P077', '1792', 'Dari', '61', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('198', 'P078', '1730', 'Wastini', '56', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('199', 'P079', '1636', 'Icih Sunarsih', '48', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('200', 'P080', '2706', 'Atin Kartini', '61', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('201', 'P081', '2355', 'Sukarna', '63', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('202', 'P082', '1252', 'Siti', '74', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('203', 'P083', '1505', 'Ernawati', '58', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('204', 'P084', '1123', 'Tasiah', '88', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('205', 'P085', '3288', 'Isnahjaeni', '18', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('206', 'P086', '2605', 'Yayah Murjanah', '60', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('207', 'P087', '2690', 'Castra Sahladi', '18', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('208', 'P088', '2025', 'Amiyati', '57', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('209', 'P089', '2190', 'Dinda M.', '15', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('210', 'P090', '1064', 'Iin', '89', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('211', 'P091', '3867', 'Nur Azizah', '51', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('212', 'P092', '1838', 'Aap Maspupah', '48', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('213', 'P093', '3912', 'Julaeha', '48', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('214', 'P094', '2984', 'Marno', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('215', 'P095', '3147', 'Yayah Nuryanah', '34', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('216', 'P096', '2610', 'Mamah', '46', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('217', 'P097', '1632', 'Syamsurizal', '63', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('218', 'P098', '3815', 'Entus', '66', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('219', 'P099', '1885', 'Caswi', '68', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('220', 'P100', '2859', 'Suhaemi', '52', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('221', 'P101', '2572', 'Hamzah', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('222', 'P102', '3593', 'Rositi', '52', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('223', 'P103', '2420', 'Yanawati', '59', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('224', 'P104', '2791', 'Nana Supriyatna', '44', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('225', 'P105', '3627', 'Ainul Hikmah', '12', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('226', 'P106', '3548', 'Enci Asriyah', '45', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('227', 'P107', '3580', 'Surjana', '71', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('228', 'P108', '2587', 'Sri Atun', '61', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('229', 'P109', '3256', 'Odirga', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('230', 'P110', '1413', 'Aesah', '49', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('231', 'P111', '2041', 'Lili', '64', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('232', 'P112', '2644', 'Nining', '67', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('233', 'P113', '1685', 'Wawan Denawan', '55', 'L', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('234', 'P114', '3506', 'Rizky Diana', '18', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('235', 'P115', '2131', 'Sri Yati', '55', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('236', 'P116', '2142', 'Emi Suhaemi', '71', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('237', 'P117', '3697', 'Isti', '63', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('238', 'P118', '3415', 'Imah', '64', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('239', 'P119', '2376', 'Titi Runingsih', '59', 'P', 'Cirebon');
INSERT INTO `tb_patient` VALUES ('240', 'P120', '2559', 'Neni', '57', 'P', 'Cirebon');

-- ----------------------------
-- Table structure for `tb_polyclinic`
-- ----------------------------
DROP TABLE IF EXISTS `tb_polyclinic`;
CREATE TABLE `tb_polyclinic` (
  `polyclinic_id` int(11) NOT NULL AUTO_INCREMENT,
  `polyclinic_cd` varchar(255) DEFAULT NULL,
  `polyclinic_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`polyclinic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_polyclinic
-- ----------------------------
INSERT INTO `tb_polyclinic` VALUES ('1', 'Poli-014', 'Mata');

-- ----------------------------
-- Table structure for `tb_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `tb_schedule`;
CREATE TABLE `tb_schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_cd` varchar(255) DEFAULT NULL,
  `schedule_day` varchar(255) DEFAULT NULL,
  `polyclinic_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `time` time DEFAULT NULL,
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_schedule
-- ----------------------------
INSERT INTO `tb_schedule` VALUES ('1', 'J001', 'Senin', '1', '1', '10:00:00');
INSERT INTO `tb_schedule` VALUES ('2', 'J002', 'Selasa', '1', '1', '10:00:00');
INSERT INTO `tb_schedule` VALUES ('3', 'J003', 'Kamis', '1', '1', '10:00:00');
INSERT INTO `tb_schedule` VALUES ('4', 'J004', 'Jumat', '1', '1', '10:00:00');
INSERT INTO `tb_schedule` VALUES ('5', 'J005', 'Sabtu', '1', '1', '11:00:00');

-- ----------------------------
-- Table structure for `tb_title`
-- ----------------------------
DROP TABLE IF EXISTS `tb_title`;
CREATE TABLE `tb_title` (
  `title_id` int(11) NOT NULL AUTO_INCREMENT,
  `title_type_id` int(11) DEFAULT NULL,
  `title_name` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT '',
  `views` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `is_navbar` int(11) DEFAULT '0',
  `seq` int(11) DEFAULT '0',
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_title
-- ----------------------------
INSERT INTO `tb_title` VALUES ('1', null, 'Project Puput', null, null, null, null, '0', '0');
INSERT INTO `tb_title` VALUES ('2', null, null, 'main', null, null, null, '0', '0');
INSERT INTO `tb_title` VALUES ('3', null, 'Login', 'main', 'login', null, null, '0', '0');
INSERT INTO `tb_title` VALUES ('4', null, 'Register', 'main', 'register', null, null, '0', '0');
INSERT INTO `tb_title` VALUES ('5', null, 'Forgot Password', 'main', 'forgotPassword', null, null, '0', '0');
INSERT INTO `tb_title` VALUES ('6', null, 'Error 404', 'main', 'error', null, null, '0', '0');
INSERT INTO `tb_title` VALUES ('7', null, 'Permission Denied', 'main', 'permission_denied', null, null, '0', '0');
INSERT INTO `tb_title` VALUES ('8', '0', 'Dashboard', 'dashboard', '', 'blank', '<i class=\"fas fa-fw fa-tachometer-alt\"></i>', '1', '1');
INSERT INTO `tb_title` VALUES ('9', '1', 'Data Pasien', 'master101', '', 'master101', '<i class=\"fas fa-address-card\"></i>', '1', '2');
INSERT INTO `tb_title` VALUES ('10', '1', 'Data Dokter', 'master201', '', 'master201', '<i class=\"fas fa-address-card\"></i>', '1', '3');
INSERT INTO `tb_title` VALUES ('11', '1', 'Data Pegawai', 'master301', '', 'master301', '<i class=\"fas fa-address-card\"></i>', '1', '4');
INSERT INTO `tb_title` VALUES ('12', '1', 'Data Poliklinik', 'master401', '', null, '<i class=\"fas fa-address-card\"></i>', '0', '5');
INSERT INTO `tb_title` VALUES ('13', '1', 'Data Jadwal', 'master501', '', null, '<i class=\"fas fa-address-card\"></i>', '0', '0');
INSERT INTO `tb_title` VALUES ('14', '2', 'Layanan 1', 'layanan101', '', null, '<i class=\"fas fa-address-card\"></i>', '1', '1');
INSERT INTO `tb_title` VALUES ('15', '2', 'Layanan 2', 'layanan201', '', null, '<i class=\"fas fa-address-card\"></i>', '1', '2');

-- ----------------------------
-- Table structure for `tb_title_type`
-- ----------------------------
DROP TABLE IF EXISTS `tb_title_type`;
CREATE TABLE `tb_title_type` (
  `title_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `title_type_name` varchar(255) DEFAULT NULL,
  `title_type_seq` int(11) DEFAULT '0',
  `title_type_visible` int(11) DEFAULT '0',
  PRIMARY KEY (`title_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_title_type
-- ----------------------------
INSERT INTO `tb_title_type` VALUES ('0', 'None', '0', '1');
INSERT INTO `tb_title_type` VALUES ('1', 'Master Data', '1', '1');
INSERT INTO `tb_title_type` VALUES ('2', 'Layanan', '3', '1');

-- ----------------------------
-- Table structure for `tb_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_full_name` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_type_id` varchar(255) DEFAULT NULL,
  `cookie` varchar(255) DEFAULT NULL,
  `is_deleted` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(255) DEFAULT NULL,
  `changed_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'moharifrifai', 'Moh Arif Rifai', 'moharifrifai@gmail.com', '123', '1', 'UMd1XeUgLjRJDgh3C4GPoxWP9yt7mvG9cC2sSIfuBapwjEbz8bzOFfZNl15h3WBK', null, null, null, null, null);
INSERT INTO `tb_user` VALUES ('2', 'puput', 'Puput', 'puput@gmail.com', '123', '1', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `tb_user_permission`
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_permission`;
CREATE TABLE `tb_user_permission` (
  `user_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user_permission
-- ----------------------------
INSERT INTO `tb_user_permission` VALUES ('1', '1', '8');
INSERT INTO `tb_user_permission` VALUES ('2', '1', '9');
INSERT INTO `tb_user_permission` VALUES ('3', '2', '6');
INSERT INTO `tb_user_permission` VALUES ('4', '2', '8');
INSERT INTO `tb_user_permission` VALUES ('5', '1', '10');
INSERT INTO `tb_user_permission` VALUES ('6', '1', '11');
INSERT INTO `tb_user_permission` VALUES ('7', '1', '12');
INSERT INTO `tb_user_permission` VALUES ('8', '1', '14');
INSERT INTO `tb_user_permission` VALUES ('9', '1', '15');

-- ----------------------------
-- Table structure for `tb_user_type`
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_type`;
CREATE TABLE `tb_user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user_type
-- ----------------------------
INSERT INTO `tb_user_type` VALUES ('1', 'Admin');
INSERT INTO `tb_user_type` VALUES ('2', 'Dokter');
